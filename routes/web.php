<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index')->name('index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// routes for user model
Route::resource('users','UserController')->middleware('admin');

// routes for product model
Route::resource('products','ProductController')->middleware('admin');
Route::post('products/publish/{id}','ProductController@publish')->middleware('admin')->name('products.publish');
Route::post('products/{id}/add_variant','ProductController@add_variant')->middleware('admin')->name('products.add_variant');
Route::post('products/{id}/edit_variant','ProductController@edit_variant')->middleware('admin')->name('products.edit_variant');
Route::delete('products/{id}/delete_variant','ProductController@delete_variant')->middleware('admin')->name('products.delete_variant');

// routes for category model
Route::resource('categories','CategoryController')->middleware('admin');

// routes for area model
Route::post('areas/store','AreaController@store')->middleware('admin')->name('areas.store');

