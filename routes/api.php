<?php

use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\ShoppingCart;
use App\Order;
use App\OrderProduct;
use App\Address;
use App\Favourite;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/**
 * Routes using API ID and KEY
 */
Route::middleware('wechat_api')->namespace('Api')->group(function(){
    /*
     * Product Model Related
     */
    Route::prefix('products')->group(function(){
        Route::get('/','ProductController@index');
    });

});
