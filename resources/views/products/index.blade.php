@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            @include('products.product_tab')
        </div>
        <hr>
        @include('layouts.feedback')
        @component('layouts.table',[
            'key_array'=>['name','img_url','sale','category_id','published'],
            'key_name'=>['名称','图片','销量','类别','是否发布','操作'],
            'data_array'=>$products,
            'show_link'=>'products.show'
        ])

        @endcomponent
    </div>
@endsection