@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('products.product_tab')
            <hr>
            <div class="col-md-8">
                @component('products.form',["action"=>"edit","product"=>$product,"categories"=>$categories])
                    @slot('title') 编辑产品 @endslot
                    @slot('link'){{route('products.update',['id'=>$product->id])}}@endslot
                @endcomponent
            </div>
        </div>

    </div>
@endsection