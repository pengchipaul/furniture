@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('products.product_tab')
            <hr>
            <div class="col-md-8">
                @component('products.form',["action"=>"create","categories"=>$categories])
                    @slot('title') 添加产品 @endslot
                    @slot('link'){{route('products.store')}}@endslot
                @endcomponent
            </div>
        </div>
    </div>

@endsection
