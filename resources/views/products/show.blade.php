@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            @include('products.product_tab')
        </div>
        <hr>
        @include('layouts.feedback')
        <h3 class="text-right">
            <form method="post" action="{{route('products.publish',['id'=>$product->id])}}">
                @csrf
                <button type="submit" class="btn btn-success">@if($product->published) 取消发布 @else 发布 @endif</button>
            </form>
        </h3>
        <h3 class="text-center mt-5">产品信息</h3>
        @component('layouts.table',[
            'key_array'=>['name','sale','category_id','published'],
            'key_name'=>['名称','销量','类别','是否发布','操作'],
            'data_array'=>array($product),
            'edit_link'=>'products.edit'
        ])
        @endcomponent
        <h3 class="text-center mt-5">产品规格</h3>
        @if($product->variants->isEmpty())

        @else
            @component('layouts.table',[
                        'key_array'=>['price','RRP','spec','dimension','color','material','stock'],
                        'key_name'=>['价格','原价','规格','尺寸','颜色','材质','库存','操作'],
                        'data_array'=>$product->variants
                    ])
            @endcomponent
        @endif
        <h3 class="text-center">
            <button class="btn btn-primary" data-toggle="modal" data-target="#add-variant">添加规格</button>
            @if($product->variants->isNotEmpty())<button class="btn btn-secondary" data-toggle="modal" data-target="#edit-variant">批量修改</button>
                 @endif
        </h3>
        @if($product->variants->isNotEmpty())
            @component('layouts.modal',[
                'modal_id'=>'edit-variant',
                'title'=>'修改规格',
                'size'=>'modal-lg'
            ])
                @slot('content')
                    <div class="card">
                        <div class="card-body">
                            <form method="post" action="{{route('products.edit_variant',['id'=>$product->id])}}">
                                @csrf
                                <table class="table table-bordered table-hover text-center">
                                    <thead>
                                    <tr>
                                        <th>价格</th>
                                        <th>原价（RRP）</th>
                                        <th>规格</th>
                                        <th>尺寸</th>
                                        <th>颜色</th>
                                        <th>材质</th>
                                        <th>库存</th>
                                        <th>是否发布（暂不可用）</th>
                                    </tr>
                                    </thead>
                                    @foreach($product->variants as $variant)
                                        <tr>
                                            <td><input class="form-control" type="number" step="1"
                                                       value="{{$variant->price}}"
                                                       name="variant_{{$variant->id}}[price]"></td>
                                            <td><input class="form-control" type="number" step="1"
                                                       value="{{$variant->RRP}}" name="variant_{{$variant->id}}[RRP]">
                                            </td>
                                            <td><input class="form-control" type="text" step="1"
                                                       value="{{$variant->spec}}" name="variant_{{$variant->id}}[spec]">
                                            </td>
                                            <td><input class="form-control" type="text" value="{{$variant->dimension}}"
                                                       name="variant_{{$variant->id}}[dimension]"></td>
                                            <td><input class="form-control" type="text" value="{{$variant->color}}"
                                                       name="variant_{{$variant->id}}[color]"></td>
                                            <td><input class="form-control" type="text" value="{{$variant->material}}"
                                                       name="variant_{{$variant->id}}[material]"></td>
                                            <td><input class="form-control" type="number" value="{{$variant->stock}}"
                                                       name="variant_{{$variant->id}}[stock]"></td>
                                            <td>
                                                <input class="form-check mx-auto" type="checkbox" checked>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                                <h3 class="text-center">
                                    <button type="submit" class="btn btn-info">更新</button>
                                </h3>
                            </form>
                        </div>
                    </div>
                @endslot
            @endcomponent
        @endif

        @component('layouts.modal',[
            'modal_id'=>'add-variant',
            'title'=>'添加规格',
            'size'=>'modal-lg'
        ])
            @slot('content')
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{route('products.add_variant',['id'=>$product->id])}}">
                            @csrf
                            <div class="form-group row">
                                <label for="price" class="col-md-4 col-form-label text-md-right">价格</label>

                                <div class="col-md-6">
                                    <input type="number" step="1"
                                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           name="price"
                                           required
                                           autofocus>

                                    @if ($errors->has('price'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="RRP" class="col-md-4 col-form-label text-md-right">原价（RRP）</label>

                                <div class="col-md-6">
                                    <input type="number" step="1"
                                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           name="RRP"
                                           required
                                           autofocus>

                                    @if ($errors->has('RRP'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('RRP') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="spec" class="col-md-4 col-form-label text-md-right">规格（选填）</label>

                                <div class="col-md-6">
                                    <input type="text"
                                           class="form-control{{ $errors->has('spec') ? ' is-invalid' : '' }}"
                                           name="spec"
                                           autofocus>

                                    @if ($errors->has('spec'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('spec') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="dimension" class="col-md-4 col-form-label text-md-right">尺寸（选填）</label>

                                <div class="col-md-6">
                                    <input type="text"
                                           class="form-control{{ $errors->has('dimension') ? ' is-invalid' : '' }}"
                                           name="dimension"
                                           autofocus>

                                    @if ($errors->has('dimension'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('dimension') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="color" class="col-md-4 col-form-label text-md-right">颜色（选填）</label>

                                <div class="col-md-6">
                                    <input type="text"
                                           class="form-control{{ $errors->has('color') ? ' is-invalid' : '' }}"
                                           name="color"
                                           autofocus>

                                    @if ($errors->has('color'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('color') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="material" class="col-md-4 col-form-label text-md-right">材质（选填）</label>

                                <div class="col-md-6">
                                    <input type="text"
                                           class="form-control{{ $errors->has('material') ? ' is-invalid' : '' }}"
                                           name="material"
                                           autofocus>

                                    @if ($errors->has('material'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('material') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="stock" class="col-md-4 col-form-label text-md-right">库存</label>

                                <div class="col-md-6">
                                    <input type="number" step="1"
                                           class="form-control{{ $errors->has('stock') ? ' is-invalid' : '' }}"
                                           name="stock" required
                                           autofocus>

                                    @if ($errors->has('stock'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('stock') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        添加
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            @endslot
        @endcomponent

        <h3 class="text-center mb-5 mt-5">商品图片展示</h3>
        @component('layouts.image_gallery',[
            'gallery_id'=>'product-imgs',
            'data_array'=>explode("|",$product->img_url),
            'width'=>'40%'
        ])
        @endcomponent
        <h3 class="text-center mt-5">商品详情展示</h3>
        @if($product->description)
            @component('layouts.image_gallery',[
                        'gallery_id'=>'description-imgs',
                        'data_array'=>explode("|",$product->description),
                        'width'=>'40%'
                    ])
            @endcomponent
        @endif

    </div>
@endsection