<div class="card">
    <div class="card-header">{{$title}}</div>
    <div class="card-body">
        <form method="post" action="{{$link}}" enctype="multipart/form-data">
            @csrf
            @if($action == "edit")
                <input type="hidden" name="_method" value="PATCH">
            @endif
            @include('layouts.feedback')
            <div class="form-group row">
                <label for="img_url" class="col-md-4 col-form-label text-md-right">@if($action=="edit")更新 @endif
                    产品图片</label>

                <div class="col-md-6">
                    <input type="file"
                           class="form-control{{ $errors->has('img_url') ? ' is-invalid' : '' }}"
                           name="img_url[]" @if($action == "create") required @endif multiple autofocus>

                    @if ($errors->has('img_url'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('img_url') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">名称</label>

                <div class="col-md-6">
                    <input id="name" type="text"
                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                           name="name"
                           value="@if($action == "create"){{ old('name') }}@else{{$product->name}}@endif" required
                           autofocus>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="title" class="col-md-4 col-form-label text-md-right">副名称（选填）</label>

                <div class="col-md-6">
                    <input type="text"
                           class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                           name="title"
                           value="@if($action == "create"){{ old('name') }}@else{{$product->title}}@endif"
                           autofocus>

                    @if ($errors->has('title'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="service" class="col-md-4 col-form-label text-md-right">服务</label>

                <div class="col-md-6">
                    <input type="text"
                           class="form-control{{ $errors->has('service') ? ' is-invalid' : '' }}"
                           name="service"
                           value="@if($action == "create"){{ old('service') }}@else{{$product->service}}@endif"
                           required autofocus>

                    @if ($errors->has('service'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('service') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="delivery" class="col-md-4 col-form-label text-md-right">物流</label>

                <div class="col-md-6">
                    <input type="text"
                           class="form-control{{ $errors->has('delivery') ? ' is-invalid' : '' }}"
                           name="delivery"
                           value="@if($action == "create"){{ old('delivery') }}@else{{$product->delivery}}@endif"
                           required autofocus>

                    @if ($errors->has('delivery'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('delivery') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="spec" class="col-md-4 col-form-label text-md-right">规格</label>

                <div class="col-md-6">
                    <input type="text"
                           class="form-control{{ $errors->has('spec') ? ' is-invalid' : '' }}"
                           name="spec"
                           value="@if($action == "create"){{ old('spec') }}@else{{$product->spec}}@endif" required
                           autofocus>

                    @if ($errors->has('spec'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('spec') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="description" class="col-md-4 col-form-label text-md-right">@if($action=="edit") 更新 @endif
                    详情介绍（图片）</label>

                <div class="col-md-6">
                    <input id="description" type="file"
                           class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                           name="description[]" multiple autofocus>

                    @if ($errors->has('description'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="detail_text" class="col-md-4 col-form-label text-md-right">@if($action=="edit") 更新 @endif
                    详情介绍（文字，选填）</label>

                <div class="col-md-6">
                    <textarea id="detail_text"
                           class="form-control{{ $errors->has('detail_text') ? ' is-invalid' : '' }}"
                              name="detail_text" autofocus>@if($action == 'edit'){{$product->detail_text}}@else{{ old('detail_text') }}@endif</textarea>

                    @if ($errors->has('detail_text'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('detail_text') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="price" class="col-md-4 col-form-label text-md-right">类别</label>

                <div class="col-md-6">
                    <select id="category_id"
                            class="form-control{{ $errors->has('category_id') ? ' is-invalid' : '' }}"
                            name="category_id" required autofocus>
                        <option>请选择类别</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}"
                                    @if($action == "edit" && $product->category_id == $category->id) selected @endif >{{$category->area->name}}
                                - {{$category->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('category_id'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary btn-block">
                        @if($action == "create") 添加 @else 更新 @endif
                    </button>
                </div>
            </div>

        </form>
    </div>
</div>