@extends('layouts.app')

@section('content')
    <div class="container-fluid">
            <div class="row justify-content-center">
                @include('categories.category_tab')
            </div>

        <hr>
        @include('layouts.feedback')
        <table class="table table-bordered table-hover text-center">
            <thead>
            <tr>
                <td>
                    <h1>房间类别名称</h1>
                </td>
                <td>
                    <h1>/</h1>
                </td>
                <td>
                    <h1>/</h1>
                </td>
                <td>
                    <h1>操作</h1>
                </td>
            </tr>
            </thead>
            @foreach($areas as $area)
                <tr>
                    <td><h2>{{$area->name}}</h2></td>
                    <td></td>
                    <td></td>
                    <td>
                        <div class="btn-group-vertical d-flex">
                            <button class="btn btn-info">编辑</button>
                            <button class="btn btn-info">显示</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><h2>类别名称</h2></td>
                    <td><h2>图片</h2></td>
                    <td><h2>操作</h2></td>
                </tr>
                @foreach($area->categories as $category)
                    <tr>
                        <td></td>
                        <td><h3>{{$category['name']}}</h3></td>
                        <td class="d-flex justify-content-center"><a href="/furniture/storage/app/{{$category['img_url']}}"  target="_blank"><img src="/furniture/storage/app/{{$category['img_url']}}"
                                                                          style="width:100px;height:100px"> </a> </td>
                        <td>
                            <div class="btn-group-vertical d-flex">
                                <a href="{{action('CategoryController@show', $category['id'])}}" class="btn btn-info">显示</a>
                                <a href="{{action('CategoryController@edit', $category['id'])}}" class="btn btn-info">编辑</a>
                            </div>
                        </td>
                    </tr>

                @endforeach
            @endforeach



        </table>
    </div>

@endsection
