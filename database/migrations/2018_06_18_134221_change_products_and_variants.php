<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeProductsAndVariants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products',function(Blueprint $table){
            $table->string('detail_text',400)->nullable();
        });

        Schema::table('variants',function(Blueprint $table){
            $table->string('spec','50')->nullable();
            $table->integer('stock')->default(0);
            $table->string('material','50')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products',function(Blueprint $table){
            $table->dropColumn('detail_text');
        });

        Schema::table('variants',function(Blueprint $table){
            $table->dropColumn('spec');
            $table->dropColumn('stock');
            $table->dropColumn('material');
        });
    }
}
