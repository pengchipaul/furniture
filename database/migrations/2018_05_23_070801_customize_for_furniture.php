<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomizeForFurniture extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products',function (Blueprint $table){
            $table->dropColumn('price');
            $table->string('img_url','300')->change();
            $table->string('description','300')->change();
            $table->string('service','100');
            $table->string('delivery','100');
            $table->string('title','100')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products',function(Blueprint $table){
            $table->float('price');
            $table->string('img_url','100')->change();
            $table->dropColumn('description','100')->change();
            $table->dropColumn('service');
            $table->dropColumn('delivery');
            $table->dropColumn('title');
        });
    }
}
