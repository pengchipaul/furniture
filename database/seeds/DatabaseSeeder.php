<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
            'email' => 'pengchipaul@gmail.com',
            'password' => Hash::make('test123'),
            'admin' => 1,
            'wechat_id' => 'admin'
        ]);
        User::create([
            'name' => 'testing',
            'email' => 'testing@tangqi.com.au',
            'password' => Hash::make('test123'),
            'admin' => 0,
            'wechat_id' => 'testing'
        ]);

    }
}
