<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{

    protected $fillable = ['name'];

    public function categories(){
        return $this->hasMany('App\Category');
    }

    public function products(){
        return $this->hasManyThrough('App\Product','App\Category');
    }
}
