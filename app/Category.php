<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //

    protected $fillable =['name','img_url'];

    public function products(){
        return $this->hasMany('App\Product');
    }

    public function area(){
        return $this->belongsTo('App\Area');
    }
}
