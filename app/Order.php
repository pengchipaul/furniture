<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $fillable = [
        'user_id', 'total', 'quantity'
    ];

    public function products(){
        return $this->belongsToMany('App\Product','order_products');
    }

    public function address(){
        return $this->belongsTo('App\Address');
    }
}
