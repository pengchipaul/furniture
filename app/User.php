<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','wechat_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin(){
        if($this->admin == 1){
            return true;
        } else {
            return false;
        }
    }

    public function cart_products(){
        return $this->belongsToMany('App\Product','shopping_carts');
    }

    public function shopping_cart(){
        return $this->hasMany('App\ShoppingCart');
    }

    public function fav_products(){
        return $this->belongsToMany('App\Product','favourites');
    }

    public function orders(){
        return $this->hasMany('App\Order');
    }

    public function addresses(){
        return $this->hasMany('App\Address');
    }

}
