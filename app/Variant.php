<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    protected $fillable=['product_id','dimension','color','price','RRP','spec','stock','material'];
}
