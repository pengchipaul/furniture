<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ShoppingCart;

class Product extends Model
{
    protected $fillable =['name','description','img_url','sale','category_id','service','delivery','title','spec','detail_text'];

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function variants(){
        return $this->hasMany('App\Variant');
    }

    public function get_cart_quantity($user_id){
        return ShoppingCart::where('user_id', $user_id)->
            where('product_id',$this->id)->firstOrFail()->quantity;
    }

}
