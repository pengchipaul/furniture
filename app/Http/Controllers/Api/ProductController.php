<?php

namespace App\Http\Controllers\Api;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    /**
     * Fetch all products that are published
     */
    public function index(){
        $products = Product::where('published',true)->get();
        foreach($products as $product){
            try{
                $product['variant'] = $product->variants->first();
            } catch(\Exception $e){
                Log::debug($e);
                return response()->json(['message'=>'error'],200);
            }

        }
        return response()->json(['message'=>'success','data'=>$products],200);
    }
}
