<?php

namespace App\Http\Controllers;

use App\Area;
use Illuminate\Http\Request;

class AreaController extends Controller
{
    public function store(Request $request){
        Area::create($request->all());
        return redirect(route('categories.index'))->with('success','创建房间成功');

    }
}
