<?php

namespace App\Http\Controllers;

use App\Variant;
use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        foreach ($products as $product) {
            $product->img_url = explode("|",$product->img_url)[0];
            $product->published = boolval($product->published);
        }
        return view('products.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('products.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if($files = $request->file('img_url'))
        {
            $images_path = array();
            try{
                foreach($files as $file){
                    if($file->isValid()){
                        $img_path = $file->store('products/images');
                        $images_path[] = $img_path;
                    } else {
                        return redirect(route('products.index'))->with('fail','图片上传失败');
                    }
                }
                $images_path = implode("|",$images_path);
            } catch(\Exception $e){
                foreach($images_path as $img_path){
                    Storage::delete('products/images/'.$img_path);
                }

            }
        } else {
            return redirect(route('products.index'))->with('fail','图片上传失败');
        }
        if($files = $request->file('description'))
        {
            $description_path = array();
            try{
                foreach($files as $file){
                    if($file->isValid()){
                        $img_path = $file->store('products/description');
                        $description_path[] = $img_path;
                    } else {
                        return redirect(route('products.index'))->with('fail','图片上传失败');
                    }
                }
                $description_path = implode("|",$description_path);
            } catch(\Exception $e){
                foreach($description_path as $img_path){
                    Storage::delete('products/description/'.$img_path);
                }

            }
        }

        if(isset($images_path)){
            $data['img_url'] = $images_path;
        }
        if(isset($description_path)){
            $data['description'] = $description_path;
        }
        Product::create($data);
        return redirect('products')->with('success','已添加产品');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        $product->published = boolval($product->published);
        $categories = Category::all();
        return view('products.show',compact('product','categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $product = Product::find($id);
        return view('products.edit',compact('categories','product','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $data = $request->all();
        unset($data['img_url']);
        unset($data['description']);
        if($files = $request->file('img_url'))
        {
            $images_path = array();
            try{
                foreach($files as $file){
                    if($file->isValid()){
                        $img_path = $file->store('products/images');
                        $images_path[] = $img_path;
                    } else {
                        return redirect(route('products.index'))->with('fail','图片上传失败');
                    }
                }
                $images_path = implode("|",$images_path);
            } catch(\Exception $e){
                foreach($images_path as $img_path){
                    Storage::delete('products/images/'.$img_path);
                }
                return redirect(route('products.index'))->with('fail','图片上传失败');
            }
        }
        if($files = $request->file('description'))
        {
            $description_path = array();

            try{
                foreach($files as $file){
                    if($file->isValid()){
                        $img_path = $file->store('products/description');
                        $description_path[] = $img_path;
                    } else {
                        return redirect(route('products.index'))->with('fail','图片上传失败');
                    }
                }
                $description_path = implode("|",$description_path);
            } catch(\Exception $e){
                foreach($description_path as $img_path){
                    Storage::delete('products/description/'.$img_path);
                }

            }
        }
        if(isset($description_path)){
            $data['description']=$description_path;
        }
        if(isset($images_path)){
            $data['img_url']=$images_path;
        }
        $product->update($data);

        return redirect('products')->with('success','已更新产品');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function add_to_cart(){
        
    }

    public function add_variant(Request $request,$id){
        $data = $request->all();
        $data['product_id'] = $id;
        try{
            Variant::create($data);
            return redirect(route('products.show',['id'=>$id]))->with('success','规格创建成功');
        }catch (\Exception $e){
            return redirect(route('products.show',['id'=>$id]))->with('fail','规格创建失败');
        }
    }

    public function edit_variant(Request $request,$id){
        $product = Product::find($id);
        try{
            foreach($product->variants as $variant){
                $data = $request->get("variant_".$variant->id);
                $variant->update($data);
            }
            return redirect(route('products.show',['id'=>$id]))->with('success','规格修改成功');
        } catch (\Exception $e){
            return redirect(route('products.show',['id'=>$id]))->with('fail','规格修改失败，部分数据出错');
        }

    }

    public function delete_variant(Request $request,$id){

    }

    public function publish($id){
        $product = Product::find($id);
        if($product->published){
            $product->published = false;
            $product->save();
            return redirect(route('products.show',['id'=>$product->id]))->with('success','已取消发布');
        } else {
            if($product->variants->isEmpty()){
                return redirect(route('products.show',['id'=>$product->id]))->with('fail','请先添加规格再发布');
            } else {
                $product->published = true;
                $product->save();
                return redirect(route('products.show',['id'=>$product->id]))->with('success','已发布');
            }
        }
    }

}
